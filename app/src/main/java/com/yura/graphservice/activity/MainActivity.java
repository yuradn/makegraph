package com.yura.graphservice.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cardiomood.android.controls.gauge.SpeedometerGauge;
import com.yura.graphservice.R;
import com.yura.graphservice.activity.MakeGraph.GraphHello;
import com.yura.graphservice.service.MyService;

public class MainActivity extends Activity {

    private static String TAG = MainActivity.class.getSimpleName();

    public static final String ACTION_START = "start";
    public static final String FUNCTION_MATH = "math";
    public static final String SET_HZ = "set_hz";
    public static final String SET_NUMBER_PAINTS = "set_number_paints";
    public static final String CHANGE_COLOR = "change_color";
    public static final String CHANGE_LINE_WIDTH = "change_line_width";
    public static final String BROADCAST_SERVICE = "com.yura.graphservice.service.generate";
    

    private double x=1;
    private MyReceiver myReceiver;

    //Make graphics object
    private GraphHello graphHello;

    // Speedometer
    private SpeedometerGauge speedometer;

    // HZ
    private int hZ = 50;

    // Line width
    private int lineWidth = 3;

    //Maximum points in screen
    private int maxPoints =500;

    //Color lines
    private String colorLine = "#fffffff8";

    private TextView textViewHz, textViewPoints, textViewMaxPoints;
    private Button colorIs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.land_layout);

        // Load preferences
        loadOnStartup();

        // Starting local service
        Intent intent = new Intent(this, MyService.class);
        intent.setAction(ACTION_START);
        startService(intent);

        // Register receiver
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BROADCAST_SERVICE);
        registerReceiver(myReceiver, intentFilter);

        // Register make graph object
        LinearLayout layoutGraph = (LinearLayout) findViewById(R.id.layoutGraph);
        graphHello = new GraphHello(layoutGraph, this);
        graphHello.setColor(colorLine);
        graphHello.setMaxNumberPoints(maxPoints);
        graphHello.setLineWidth(lineWidth);
        graphHello.init();


        // Customize SpeedometerGauge
        speedometer = (SpeedometerGauge)findViewById(R.id.speedometer);

        // configure value range and ticks
        speedometer.setMaxSpeed(4);
        speedometer.setMajorTickStep(1);
        speedometer.setMinorTicks(0);

        // Configure value range colors
        speedometer.addColoredRange(0, 1, Color.GREEN);
        speedometer.addColoredRange(1, 3, Color.YELLOW);
        speedometer.addColoredRange(3, 4, Color.RED);


        // Find TextViews, buttons
        textViewHz = (TextView) findViewById(R.id.textView);
        textViewPoints = (TextView) findViewById(R.id.textViewNumberPoints);
        textViewMaxPoints = (TextView) findViewById(R.id.textViewMaxPoints);

        colorIs = (Button) findViewById(R.id.buttonColorIs);

        Button buttonSettings = (Button) findViewById(R.id.button);
        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                intent.putExtra(CHANGE_COLOR, colorLine);
                intent.putExtra(SET_HZ, hZ);
                intent.putExtra(SET_NUMBER_PAINTS, maxPoints);
                intent.putExtra(CHANGE_LINE_WIDTH, lineWidth);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(TAG, "OnStart");
        textViewHz.setText("HZ: " + hZ);
        textViewPoints.setText(" "+ graphHello.getNumberPoints());
        textViewMaxPoints.setText(" "+maxPoints);
        colorIs.setBackgroundColor(Color.parseColor(colorLine));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveOnExit();
        Intent intent = new Intent(this, MyService.class);
        stopService(intent);
        try {
            unregisterReceiver(myReceiver);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction()!=null) {
                switch (intent.getAction()) {
                    case BROADCAST_SERVICE:
                        Double y = intent.getDoubleExtra(FUNCTION_MATH, 0);
                        try {
                            graphHello.make(x, y);
                        } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }

                        textViewPoints.setText(" " + graphHello.getNumberPoints());
                        textViewMaxPoints.setText(" "+maxPoints);
                        try {
                            speedometer.setSpeed(y + 2);
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                        x++;
                        break;
                }
            }
        }
    }

    private void saveOnExit(){
        SharedPreferences sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putInt(SET_HZ, hZ);
        ed.putInt(SET_NUMBER_PAINTS, maxPoints);
        ed.putString(CHANGE_COLOR, colorLine);
        ed.putInt(CHANGE_LINE_WIDTH, lineWidth);
        ed.apply();

    }

    private void loadOnStartup(){
        SharedPreferences sPref = getPreferences(MODE_PRIVATE);
        hZ = sPref.getInt(SET_HZ, 50);
        maxPoints = sPref.getInt(SET_NUMBER_PAINTS, 500);
        colorLine = sPref.getString(CHANGE_COLOR, "#fffffff8");
        lineWidth = sPref.getInt(CHANGE_LINE_WIDTH,3);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data!=null) {
            try {
                String recentColor = data.getStringExtra(CHANGE_COLOR);
                // Change color if changed
                if (!recentColor.equals(colorLine)) {
                    colorLine = data.getStringExtra(CHANGE_COLOR);
                    graphHello.setColor(colorLine);
                }

                // Change HZ if changed
                int newHz = data.getIntExtra(SET_HZ, 50);
                if (newHz != hZ) {
                    hZ = newHz;
                    Intent intent = new Intent(MainActivity.this, MyService.class);
                    intent.setAction(SET_HZ);
                    intent.putExtra(SET_HZ, hZ);
                    startService(intent);
                }

                // Change maximum points if changed
                int newMaxPoints = data.getIntExtra(SET_NUMBER_PAINTS, 500);
                if (newMaxPoints != maxPoints) {
                    maxPoints = newMaxPoints;
                    graphHello.setMaxNumberPoints(maxPoints);
                }

                // Change line width if changed
                int newLineWidth = data.getIntExtra(CHANGE_LINE_WIDTH, 3);
                if (newLineWidth != lineWidth) {
                    lineWidth = newLineWidth;
                    graphHello.setLineWidth(lineWidth);
                }
            } catch (Exception e) {
                Log.d(TAG, e.toString());
                hZ = 50;
                lineWidth = 3;
                maxPoints =500;
                colorLine = "#fffffff8";
            }

        }
    }




}
