package com.yura.graphservice.activity.MakeGraph;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

public class GraphHello implements MakeGraph {

    private final static String TAG = GraphHello.class.getSimpleName();

    private LineChartView chart;
    private List<PointValue> values;
    private String color = "#fffffff8";
    private int max = 500;
    private int widthLine =1;
    LineChartData data;

    public GraphHello(LinearLayout layout, Context context) {
        chart = new LineChartView(context);
        layout.addView(chart);
    }

    @Override
    public void init() {
        values = new ArrayList<PointValue>();

        Line line = new Line(values).setColor(Color.parseColor(color)).setCubic(true);
        List<Line> lines = new ArrayList<Line>();
        lines.add(line);

        data = new LineChartData();
        data.setLines(lines);

        chart.setLineChartData(data);
    }

    @Override
    public void make(double x, double y) {
        float fX = (float) x;
        float fY;

        y =Math.round(y*1000);
        fY=Float.valueOf(Double.toString(y));

        while (values.size()+1 > max) values.remove(0);
        values.add(new PointValue(fX, fY));
        Line line = new Line(values).setColor(Color.parseColor(color)).setCubic(true).setStrokeWidth(widthLine);
        List<Line> lines = new ArrayList<Line>();
        lines.add(line);

        data = new LineChartData();
        data.setLines(lines);

        chart.setLineChartData(data);
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void setLineWidth(int w) {
        widthLine = w;
    }

    @Override
    public int getNumberPoints() {
        return values.size();
    }

    @Override
    public void setMaxNumberPoints(int max) {
        this.max = max;
    }
}