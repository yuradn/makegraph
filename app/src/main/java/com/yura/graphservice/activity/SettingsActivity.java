package com.yura.graphservice.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.yura.graphservice.R;

public class SettingsActivity extends Activity implements View.OnClickListener {
    private final static String TAG = SettingsActivity.class.getSimpleName();
    private String color = "#fffffff8";
    private int hZ = 50;
    private int numberPaints = 500;
    private int lineWidth = 3;

    private SeekBar seekBarHz, seekBarNumberPaints;

    private Button currentColorIs;
    private TextView textViewHz, textViewNumberPaints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        Intent intent = getIntent();
        if (intent!=null) {
            color = intent.getStringExtra(MainActivity.CHANGE_COLOR);
            numberPaints = intent.getIntExtra(MainActivity.SET_NUMBER_PAINTS, 500);
            hZ = intent.getIntExtra(MainActivity.SET_HZ,50);
            lineWidth = intent.getIntExtra(MainActivity.CHANGE_LINE_WIDTH,3);
            Log.d(TAG,"hz="+hZ+" paints="+numberPaints+" color="+color);
        }

        ((Button) findViewById(R.id.buttonWhite)).setOnClickListener(this);
        ((Button) findViewById(R.id.buttonPink)).setOnClickListener(this);
        ((Button) findViewById(R.id.buttonGreen)).setOnClickListener(this);
        ((Button) findViewById(R.id.buttonPaleGreen)).setOnClickListener(this);
        ((Button) findViewById(R.id.buttonYellow)).setOnClickListener(this);
        ((Button) findViewById(R.id.buttonLightBlue)).setOnClickListener(this);
        ((Button) findViewById(R.id.buttonBlue)).setOnClickListener(this);
        ((Button) findViewById(R.id.buttonRed)).setOnClickListener(this);

        currentColorIs = (Button) findViewById(R.id.buttonCurrentColorIs);
        currentColorIs.setBackgroundColor(Color.parseColor(color));

        textViewHz = (TextView) findViewById(R.id.textHz);
        textViewHz.setText(""+hZ);

        textViewNumberPaints = (TextView) findViewById(R.id.textViewNumberOfPaints);
        textViewNumberPaints.setText(""+numberPaints);

        seekBarHz = (SeekBar) findViewById(R.id.seekBarHz);
        seekBarHz.setProgress(hZ);
        seekBarHz.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > 1) {
                    hZ = progress;
                    textViewHz.setText("" + hZ);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        seekBarNumberPaints = (SeekBar) findViewById(R.id.seekBarNumberPaints);
        seekBarNumberPaints.setProgress(numberPaints);
        seekBarNumberPaints.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > 10) {
                    numberPaints = progress;
                    textViewNumberPaints.setText("" + numberPaints);
                } else seekBar.setProgress(10);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        RadioButton radioButton_1 = (RadioButton) findViewById(R.id.radio_1);
        RadioButton radioButton_3 = (RadioButton) findViewById(R.id.radio_3);
        RadioButton radioButton_5 = (RadioButton) findViewById(R.id.radio_5);
        RadioButton radioButton_7 = (RadioButton) findViewById(R.id.radio_7);
        RadioButton radioButton_10 = (RadioButton) findViewById(R.id.radio_10);
        RadioButton radioButton_12 = (RadioButton) findViewById(R.id.radio_12);
        RadioButton radioButton_15 = (RadioButton) findViewById(R.id.radio_15);

        switch (lineWidth){
            case 1:
                radioButton_1.setChecked(true);
                break;
            case 3:
                radioButton_3.setChecked(true);
                break;
            case 5:
                radioButton_5.setChecked(true);
                break;
            case 7:
                radioButton_7.setChecked(true);
                break;
            case 10:
                radioButton_10.setChecked(true);
                break;
            case 12:
                radioButton_12.setChecked(true);
                break;
            case 15:
                radioButton_15.setChecked(true);
                break;
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_1:
                if (checked)
                    lineWidth = 1;
                    break;
            case R.id.radio_3:
                if (checked)
                    lineWidth =3;
                    break;
            case R.id.radio_5:
                if (checked)
                    lineWidth =5;
                break;
            case R.id.radio_7:
                if (checked)
                    lineWidth =7;
                break;
            case R.id.radio_10:
                if (checked)
                    lineWidth =10;
                break;
            case R.id.radio_12:
                if (checked)
                    lineWidth =12;
                break;
            case R.id.radio_15:
                if (checked)
                    lineWidth =15;
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonWhite:
                color = "#fffffff8";
                break;
            case R.id.buttonPink:
                color = "#ffff03b7";
                break;
            case R.id.buttonGreen:
                color = "#ff05ff0b";
                break;
            case R.id.buttonPaleGreen:
                color = "#ff53ff8d";
                break;
            case R.id.buttonYellow:
                color = "#ffffee00";
                break;
            case R.id.buttonLightBlue:
                color = "#ff70fff2";
                break;
            case R.id.buttonBlue:
                color = "#ff020aff";
                break;
            case R.id.buttonRed:
                color = "#ffff0206";
                break;
        }
        Log.d(TAG, "Change color to: " + color);
        currentColorIs.setBackgroundColor(Color.parseColor(color));

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Log.d(TAG, "OnBackPressed");
        putResultToActivity();
        finish();
    }

    public void putResultToActivity(){
        Intent intent = new Intent();
        intent.putExtra(MainActivity.CHANGE_COLOR, color);
        intent.putExtra(MainActivity.SET_HZ, hZ);
        intent.putExtra(MainActivity.SET_NUMBER_PAINTS, numberPaints);
        intent.putExtra(MainActivity.CHANGE_LINE_WIDTH, lineWidth);
        setResult(RESULT_OK, intent);
    }

}
