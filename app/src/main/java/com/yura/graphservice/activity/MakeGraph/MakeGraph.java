package com.yura.graphservice.activity.MakeGraph;

public interface MakeGraph {
    public void init();
    public void make(double x, double y);
    public void setColor(String color);
    public void setLineWidth(int j);
    public int getNumberPoints();
    public void setMaxNumberPoints(int max);

}
