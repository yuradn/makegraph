package com.yura.graphservice.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import com.yura.graphservice.activity.MainActivity;

import java.util.Calendar;

public class MyService extends Service {

    private static String TAG = MyService.class.getSimpleName();

    private int hZ = 50;
    private long timeCycle = 60 / hZ;

    Handler mHandler;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent==null) return super.onStartCommand(intent, flags, startId);
        if (intent.getAction()!=null) {
            switch (intent.getAction()) {
                case MainActivity.ACTION_START:
                    //Log.d(TAG,"Start function startServiceWork graph.");
                    startServiceWork();
                    break;
                case MainActivity.SET_HZ:
                    hZ = intent.getIntExtra(MainActivity.SET_HZ,50);
                    timeCycle = 1000 / hZ;
                    break;
            }
        }


        return super.onStartCommand(intent, flags, startId);
    }

    private void startServiceWork() {
        timeCycle = 1000 / hZ;
        useHandler();
    }

    public void useHandler() {
        mHandler = new Handler();
        mHandler.postDelayed(mRunnable, timeCycle);
    }

    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            double generated = generateFunc();
            //Log.d(TAG, "Calls,time cycle: " + " pause: "+timeCycle+" function: "+generated);
            sendMyBroadcast(generated);
            mHandler.postDelayed(mRunnable, timeCycle);
        }
    };

    public double generateFunc(){
        double result=0;
        Calendar cal = Calendar.getInstance();
        int millisecond = cal.get(Calendar.MILLISECOND);
        double y1 = Math.sin(millisecond);
        double y2 = Math.cos(millisecond);
        result = y1 + y2;

        return result;
    }

    private void sendMyBroadcast(double generated){
        //Log.d(TAG,"Send My Broadcast");
        Intent intent = new Intent();
        intent.setAction(MainActivity.BROADCAST_SERVICE);
        intent.putExtra(MainActivity.FUNCTION_MATH, generated);
        sendBroadcast(intent);
    }

}
